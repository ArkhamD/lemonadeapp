package com.example.lemonadeapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.AbsoluteRoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults.buttonColors
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.lemonadeapp.ui.theme.LemonadeAppTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LemonadeAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    LemonadePreview()
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun LemonadePreview(modifier: Modifier = Modifier) {
    Column {
        Button(
            onClick = { /*TODO*/ },
            shape = RectangleShape,
            colors = buttonColors(MaterialTheme.colorScheme.primaryContainer),
            modifier = Modifier
                .fillMaxWidth()
                .height(65.dp)
        ) {
            Text(
                text = stringResource(R.string.lemonade),
                fontSize = 28.sp,
                fontWeight = FontWeight.Bold,
                color = Color.Black
            )
        }
        PhotoWithText(
            modifier
                .fillMaxSize()
                .wrapContentSize(Alignment.Center))
    }
}

@Composable
fun PhotoWithText(modifier: Modifier = Modifier) {
    var stage by remember {
        mutableStateOf(1)
    }

    if (stage > 8) stage = 1

    val imageResourse = when (stage) {
        1 -> R.drawable.lemon_tree
        7 -> R.drawable.lemon_drink
        8 -> R.drawable.lemon_restart
        else -> R.drawable.lemon_squeeze
    }
    
    val textResourse = when (stage) {
        1 -> R.string.lemon_tree
        7 -> R.string.drink_lemonade
        8 -> R.string.restart
        else -> R.string.squeeze
    }

    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Button(
            onClick = { stage++ },
            shape = AbsoluteRoundedCornerShape(40.dp),
            colors = buttonColors(MaterialTheme.colorScheme.tertiaryContainer)
        ) {
            Image(
                painter = painterResource(id = imageResourse),
                contentDescription = null,
                modifier = Modifier
                    .width(128.dp)
                    .height(160.dp)
            )
        }
        Spacer(
            modifier = Modifier.height(16.dp)
        )
        Text(
            text = stringResource(id = textResourse),
            fontSize = 18.sp
        )
    }
}

